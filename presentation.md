footer: @lulalala_it
slidenumbers: true

^ Hello everyone. Thanks for attending this talk.

![](grass.jpg)

---

### About me
## [fit] lulalala
otaku and happy rubyist
Open speaker in (un)conference

twitter: @lulalala_it

^ My name is Mark, and my github id is lulalala
I am an otaku from Taiwan
I draw manga,
I like to make gems
and I like Ruby!


![](grass.jpg)

---

## ActiveModel::Errors
## and
## AdequateErrors

^
Today I am going to talk about ActiveModel errors and beyond~~

![](grass.jpg)

---

# What's out of the scope?

⌧ Ruby Exception and Error Handling
⌧ Revamping ActiveModel Validation
⌧ Non-Rails model validation errors (e.g. Dry-validation)

^ to clear any misunderstanding from the beginning, I want to say that:
no, this talk is not about..

---

# Agenda

1. ActiveModel::Errors and its issues
2. AdequateErrors
3. ???
4. Profit!

---

# Familiar with this?

```ruby
person.errors[:name]
# => ["cannot be nil"]
```

^ Okay! let's get started... has everyone used this before...


---

# How about this?

```ruby
person.errors.details
# => {:name=>[
#      {error: :not_implemented},
#      {error: :invalid}
#    ]}
```

^ and also the details API introduced in Rails 4?

---

# ActiveModel::Errors

```ruby
model.errors.add
model.errors.added?
model.errors.messages
model.errors.details
model.errors.full_messages_for
model.errors.generate_message
```

^ as we all know, active model errors is Rails' API for you to access model validation errors

---

# [fit] 「我跟它八字不合」
## [fit] "Our astrological signs do not aligned with each other."

^ but you know what? I have a love hate relationship with it

---

# Simple Interface

Just arrays and hashes and strings

---

# Simple Interface

Just arrays and hashes and strings

# [fit] Simple to use ≠ easy to accomplish something

Over the years I have encountered many issues with the interface.

---

### I usually use workaround those,
### but one day, PM gave us this requirement:

---

### "Please show all error messages in full, except the `too_cool` error"
#### PM

^Please show all error messages in full, except too_coooool error
It's a reasonable request... right?

---

```ruby
details.map { |detail|
  type = detail.delete(:error)
  next if type == :too_coooool
  if type.is_a?(Symbol)
    model.errors.full_message(
      attribute, model.errors.generate_message(attribute, type, detail)
    )
  else
    model.errors.full_message(
      attribute, type
    )
  end
}.compact
```

^ Here is the solution
let's walk through it
we loop through each details.
we get the error type out of details first
if it is too cool, we skip it
then we generate message.
we check if type is a symbol or a string
we then call generate_message to get message
and finally we call full_message to get the full message

---

# [fit] That's a lot of lines to filter error messages.

---

# [fit] I am sure we can improve this?

---

![fit](collecting.png)

^ So last year, I opened a github repo,
asking people what they dislike about the interface.
I gathered some feedbacks, and think about how to fix those.

---

### And I realized that one architecture decision is the root of a lot of those issues. 

---

Active Model Errors provides **a modified Hash** that you can include in your object for handling error messages...

Rails API doc

^ here is the definition from Rails documentation

---

Active Model Errors represents **an array of Error objects** that you can include in your object for handling error messages...

lulalala

---

# Current Errors:

- `@messages` - A hash of array of Strings
- `@details` - A hash of array of hashes

# My ideal Errors:

- `@errors` - An array of Error objects

---

# Error object

- Represents **one** error

```ruby
e = Error.new(:title, :too_coooool, count: 432)
e.attribute # :title
e.type      # :too_coooool
e.options   # {count: 432}
e.message   # "is too coooool for ya"
e.full_message   # "Title is too coooool for ya"
```

---

# What are the benefits?
^ so what are the benefits of replaces the data structure here?
Let's walk through several cases one by one:

---

## Information is not all over the place.

Easier to get all data of one error

---

![fit](illus1-1.jpg)

---

![fit](illus1-2.jpg)

---

![fit](illus1-3.jpg)

---

# [fit] Message and corresponding details

^Let's look at an example. If we have two `foo_error` and one `bar_error`,
how do we get the message of the last error?

```ruby
{:name => [
  {error: :foo_error, count: 1},
  {error: :bar_error},
  {error: :foo_error, count: 3}
]}
```

### [fit] How to access the message of the last error?

^ beside doing what we did, re-creating messages, some might think we can use array indexes

```ruby
model.errors.messages[:name][2]
```

---

# [fit] Do you know message <-> details is not always 1:1?

---

![fit](illus2-1.jpg)

---

### Yes, some message may have no details

> An __equivalent__* to `errors#add` is to use `<<` to append a message to the `errors.messages` array for an attribute:
- Rails Guide

<br/>

```ruby
errors.messages[:name] << "foo"
```

<br/>

\* obviously not accurate

---

## Rails does weird things that you do not know of

For example, it calls uniq! when importing errors from associations

```ruby
errors[attribute].uniq!
errors.details[reflection_attribute].uniq!
```

^ this was a hack fix for duplicated errors

---

## Finer granularity: `delete`

Do you know why delete can only be done on attribute level?

```ruby
model.errors.delete(:title)
```

---

## Finer granularity: `delete`

- message:details are not 1:1
- can't reliably pinpoint target data.

<br/>

With Error object, we can achieve this:

```ruby
model.errors.delete(attribute: :title, type: :too_cooool)
```

---

## Data in many places causes
## [fit] Data inconsistency (not 1:1)
### and with
## Data in __one__ place, we would have less issue.

---

![fit](illus1-4.jpg)

---

# [fit] Easier Enumeration

^ With Error objects array, we can easily filter, and then render the message:

```ruby
model.
  errors.
  find_all{|e| e.options[:count] < 500}.
  map(&:message)
```

Much easier to enumerate one array,  
instead of hashes of arrays.

---

## Extensible by gem authors

A class for you to monkey patch and add functionalities.

^ we don't like to touch core classes.

---

# Benefits of Error object

1. Information in one place
2. Eliminate old issue of message:details not 1:1
3. Easier enumeration
4. Extensible

^ To summarise the benefits of representing error as an object

---













---

## Part 2:

---

^ I made a gem called adeqaute errors, to test these ideas. It is transparent, and will not break existing code.

# AdequateErrors

![fit](adequate.png)

Does not break existing code.

Experimental

New functionalities live under `model.errors.`**`adequate`**

^ New functionalities live under `model.errors.`**`adequate`**
You can play around with it if you want.

---

![fit](illus4-1.jpg)

---

![fit](illus4-2.jpg)

---

![fit](illus4-3.jpg)

---

![fit](illus4-4.jpg)

---

![fit](illus4-5.jpg)

---

# [fit] Enhanced or new functionalities

```ruby
errors.adequate.delete(count:3)
errors.adequate.messages
errors.adequate.messages_for(attribute: :title)
errors.adequate.where(type: :blank)
```

---

# Lazy message evaluation

@morgoth mentioned that it would be desirable to evaluate message lazily.

Currently:

```ruby
# default locale being :en
I18n.with_locale(:pl) { user.error.full_messages } # => still in EN
```

Desired behavior:

```ruby
I18n.with_locale(:pl) { user.error.full_messages } # => outputs PL errors
I18n.with_locale(:pt) { user.error.full_messages } # => outputs PT errors
```
---

## Finer granularity: `where` query

A convenience method to do filtering

```ruby
model.errors.where(type: :too_cool)
# returns an array of all too_cool type errors
```

---

# Sometimes we want to copy errors across models

- Validating child association using autosave association
- Form objects and service objects

---

# Autosave Association

```ruby
class School < ActiveRecord::Base
  has_many :students, autosave: true
```

When saving school, if student autosave fails, its errors are copied to School.

```ruby
{:"students.name"=>["can't be blank"]}
```

---

# [fit] Autosave Association - how is it done?

```ruby
item.errors.add(:moo, :invalid)
NoMethodError: undefined method `moo' for #<Item id: nil,
name: nil, created_at: nil, updated_at: nil>
```

Reason:  
generating message requires reading the attribute value.


^ add does not allow us to add error to non-existent attribute,
because generating message requires reading the attribute value.

So how is `"students.name"` error added to School?

---

# [fit] Autosave Association - bypass add

```ruby
record.errors.each do |attribute, message|
  errors["#{reflection.name}.#{attribute}"] << message
  errors[attribute].uniq!

...

record.errors.details[attribute].each do |error|
  errors.details[reflection_attribute] << error
  errors.details[reflection_attribute].uniq!
```

^ rails bypasses add, and copies and appends the information to messages and details hash directly.
this is also where we mentione `uniq!` earlier.
The whole thing seems a bit of a hack

---
[.build-lists: true]
# [fit] Autosave Association and Error object

How can we do it with Error objects?

- Can we copy the Error object and append it to parent?
- Message rendering would break, because parent does not have that attribute.
- **Introducing NestedError**

---

# NestedError

^ nested error is a subclass of error
it wraps around source error,
and ask source error to do message generation.

```ruby
class NestedError < Error
```

It is a wrapper for source error.

Methods such as `message` and `full_message` are forwarded to source error.

---

# `import` errors

^ I have added an import method. People can use this instead of add, in order to create nested errors.

It's like `add`, but takes in an Error and wraps it as NestedError 

```ruby
def import(error, override_options = {})
  @errors.append(
    NestedError.new(@base, error, override_options)
  )
end
```

---

# Flexibility with full message attribute prefix

```ruby
validates_acceptance_of :terms_of_service,
  message: 'Please accept the terms of service'
```

We get:

**"Terms of Service Please accept the terms of service"**

^ First, Full message always prefixed with attribute name
it's not uncommon for PM to want a more friendly error message.

---

# Can we assign it to :base ?

```ruby
errors.add(:base, :foo)
```

But we can't here because we are using default validator.

---

# Can we monkey-patch it?

`custom_error_message` gem detects if message starts with `^`, and will skip prefix if it is.

```ruby
validates_acceptance_of :terms_of_service,
  message: '^Please accept the terms of service'
```

But this can have edge case bug too.

---

# Solution: full message only

error.message always display full message.

```yaml
messages:
  accepted: "must be accepted"
```

...is changed to:

```yaml
messages:
  accepted: "%{attribute} must be accepted"
```

---








## Part 3:
# ???

---

## AdequateErrors was done. 
## I am happy, so I took the next step.

---

![fit](rails_pr.png)

^ I decided to write a pull request to Rails

---

### Rails 5.2 is out, and next version is Rails 6.

---

### It's more likely to accept bigger changes
#### (my guess)

---

## Break as little things as possible

^ Adequate errors is a bit radical,
and I know those radical parts won't be accepted into Rails
so I need to somehow change its structure, adding new functionalities, and make sure I break very little stuff.

Feature | Adequate Errors | Pull Request |
--------|----------------|-------
Error object | Yes | Yes |
Lazy message evaluation | Yes | Yes |
Full message always | Yes | |
Nested Error | Yes | Yes |
`where` query | Yes | Slightly different |


---

# Each

I want errors to enumerate like an array:

```ruby
model.errors.each { |error| }
```

But we also want to keep existing functionality:

```ruby
model.errors.each { |attribute, message| }
```

---

# Each - check arity

```ruby
def each(&block)
  if block.arity == 1
    @errors.each(&block)
  else
    raise_depreacation_warning
    
    @errors.each { |error|
      yield error.attribute, error.message
    }
  end
end
```

---

# Each and Enumerable

```ruby
class Errors
  include Enumerable
```

40+ methods (e.g. `reject`, `map`, `uniq`) are available through `each`.

^ Errors class includes enumerable
therefore all enumerable methods are available, through the `each` method.
Since we took care of each, these are taken care of as well.

---

Our `each` is not very fast,  
therefore enumerable methods are not very fast.

We can speed up some methods by forwarding it to Error array directly.

![original](illus3.jpg)

---

# Each - ordering

`message` and `details` are attribute-keyed hashes:

`each` iteration are kind of sorted (grouped) by attribute.

```ruby
@errors.
  sort { |a, b| a.attribute <=> b.attribute }.
  each { |error| yield error.attribute, error.message }
```

---

## Finer granularity: `added?`

^ If I have this error, When checking if error exists, I must supply identical inputs

```ruby
errors.add(:title, :too_short, count: 3)
errors.added?(:title, :too_short, count: 3) # true

# Rails 5.2:
errors.added?(:title, :too_short) # false

# PR:
errors.added?(:title, :too_short) # true
```

---

## Finer granularity: `delete` 

```ruby
errors.delete(:title, :too_short)
errors.delete(:title, :too_short, count: 3)
```

---


## Do you know hash and array can have default value by using Proc?

```ruby
h = Hash.new { |h, key| h[key] = [] }
h[404]
h # {404=>[]}
```

---

## Rails uses this so you can append hash dynamically

```ruby
errors[:title] << 'bar'
```

### But this cause it to be unserializable

---

# Serialization - Marshal

So in Rails 5, custom marshal is defined to remove and add default proc.

```ruby
def marshal_dump # :nodoc:
  [@base, without_default_proc(@messages), without_default_proc(@details)]
end
def marshal_load(array) # :nodoc:
  @base, @messages, @details = array
  apply_default_array(@messages)
  apply_default_array(@details)
end
```

^ [after reading slides]
as you can see, it serializes the object as an array instead.

---

# Serialization - Marshal

In my PR, we no longer allows

```ruby
errors[:title] << 'bar'
```

We can remove complexities related to procs and custom marshaling.

---

# Removing code always makes a coder happy :)

---

# [fit] Serialization - Compatibility

Must support loading serializations from Rails 5.

We add `marshal_dump` and `marshal_load` to customize it.

But what will happen if we remove those?

---

# [fit] Serialization - Compatibility

Ruby knows what's serialized by custom `marshal_dump`, and what's serialized by build-in `Marshal`.

So we can remove `marshal_dump`,  
and keep `marshal_load` to load Rails 5 dumps.

---

![](illus5-1.jpg)

---

![](illus5-2.jpg)

---
[.build-lists: true]
# Serialization - YAML

- Guess how YAML serialization can be customized?
- `yaml_load`?
- Wrong! `init_with`
- Totally different interfaces  
  (Rantings: could the interface for Marshal and YAML be unified?)

---

# Deprecation

- `each` and enumerable friends (when used as hash)
- `values`, `keys`
- `to_xml` (a very very old method)
- `full_message`
- `generate_message`
- `[]` (use message_for instead)

---

Anyways, a lot of things needs to be fixed in the PR.

- dup and clone

Just takes some time to fix one by one...

---

### Okay, let's wrap up here~~
## Part 4: Profit!

---

# [fit] Let's return to the very first example

```ruby
errors.details.map { |detail|
  type = detail.delete(:error)
  next if type == :too_coooool
  if type.is_a?(Symbol)
    model.errors.full_message(
      attribute, 
      model.errors.generate_message(attribute, type, detail)
    )
  else
    model.errors.full_message(
      attribute, type
    )
  end
}.compact
```

---

```ruby
errors.
  reject { |error| error.type == :too_cooool }.
  map(&:full_message)
```

---

^ Ok! hopefully by now I've convinced you that this is the way to go
so if you are interested
Currently the discussion died down a bit.
I think they were busy releasing Rails 5.2 and organizing Rails Conf.
Now these things are over, there should be activities again.

## github.com/rails/rails/pull/32313

Discuss about API design, e.g.:
<br/>
`where(attribute: :title, type: :too_coool)`
vs
`where(:title, :too_coool)`

<br/>

or just...

---

^ click on the thumb up in the PR, thank you.

![fit](adequate.png)
